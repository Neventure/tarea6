import axios from "axios";

const baseURL = "http://170.239.85.65:3000/";

export function jobList() {
  return axios.get(`${baseURL}/jobs`);
}
export function jobDetails(id){
  return axios.get(`${baseURL}/jobs/${id}`);
}